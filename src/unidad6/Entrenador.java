package unidad6;

import java.util.ArrayList;

public class Entrenador {
	private String nombre;
	private ArrayList <Pokemon> pokemons;
	private int insignias;

	//	Constructor sólo con el nombre y una colección vacia de Pokemons
	public Entrenador(String nombre) {

		this.nombre = nombre;
		pokemons = new ArrayList<>();
		insignias = 0;

	}
	//  Constructor con colección inicial de pokemons
	
	public Entrenador(String nombre, ArrayList<Pokemon> pokemons) {
		this.nombre = nombre;
//		Creo una colección nueva a partir de la colección de Pokemons que recibo como parámetro.
		this.pokemons = new ArrayList<>(pokemons);
		insignias = 0; 
	}
//	  	Constructor par aun entrenador como un número de insignias inicial.
	public Entrenador(String nombre, ArrayList<Pokemon> pokemons, int insignias) {
		this.nombre = nombre;
		this.pokemons = pokemons;
		this.insignias = insignias;
	}

	public String getNombre() {
		return nombre;
	}

	public ArrayList<Pokemon> getPokemons() {
		return pokemons;
	}

	public int getInsignias() {
		return insignias;
	}

	public void setInsignias(int insignias) {
		this.insignias = insignias;
	}
//	Método para añadir Pokemons a la colección
	public void addPokemon(Pokemon p) {
		pokemons.add(p);
		
	}

}
