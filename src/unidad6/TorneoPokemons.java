package unidad6;

import java.util.ArrayList;
import java.util.Scanner;

public class TorneoPokemons {

	private static Scanner in = new Scanner(System.in);
	//		para almacenar entrenadores, un arrayList
	private static ArrayList<Entrenador> entrenadores = new ArrayList<>(); 

	public static void main(String[] args) {
		//		Punto de entrada al programa, aquí implemento toda la lógica del juego

		// 		Dentro del bucle while se desarrolla el torneo
		while(jugarTorneo()) {
			obtenerEntrenadores();

		}
	}
              
	static void obtenerEntrenadores() {
		String linea = in.nextLine();
		while(!linea.equalsIgnoreCase("torneo")) {

		}

	}

	static boolean jugarTorneo() {
		boolean jugarTorneo;
		boolean correcta;

		do {
			System.out.print("¿Jugar un torneo? (s/n): ");
			String respuesta = in.nextLine().toLowerCase();
			correcta = (jugarTorneo = respuesta.equals("s")) || respuesta.equals("n");
			if (!correcta)
				System.out.println("Respuesta incorrecta");
		} while (!correcta);
		return jugarTorneo;
	}

}


