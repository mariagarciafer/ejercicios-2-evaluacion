package unidad6;

import java.util.Random;

public class Mascota {
	
		public static enum Estado {SANA, ENFERMA, FALLECIDA};

		private static  Random r = new Random();
		private String nombre; 
		private int energia = 20;
		
		public Mascota(String nombre) {
			
			this.nombre = nombre;
		}
		public String comer() {
			if (estado() == Estado.FALLECIDA)
				new Exception("La mascota ha fallecido");
			if (r.nextInt(10)<3 && estado() == Estado.SANA && energia >10)
				energia = 10;
			else if (estado() == Estado.ENFERMA)
				energia--;
			else
				energia += 5;
			return estadoAnimo();
		}
		
		public String dormir() {
			if (estado() == Estado.FALLECIDA)
				new Exception("La mascota ha fallecido");
			if (estado() == Estado.ENFERMA)
				energia--;
			else
				energia += 2;
			return estadoAnimo();
		}
		public String hacerEjercicio() {
			if (estado() == Estado.FALLECIDA)
				new Exception("La mascota ha fallecido");
			energia-=3;
			if (estado() == Estado.ENFERMA)
				energia--;
			else
			energia -=3;
			
			return estadoAnimo();
		}
		
		public Estado estado() {
			if (energia >=5 && energia <=50)
				return Estado.SANA;
			else if (energia >=0 || energia <=55)
				return Estado.ENFERMA;
			else
				return Estado.FALLECIDA;
			
			
		}
		public void curar() {
			if (estado() == Estado.FALLECIDA)
				new Exception("La mascota ha fallecido");
			if (estado() == Estado.ENFERMA)
				energia = 20;
		}
//		Revisar estos niveles de energia:                                                                                                                                                                                                                                                                                               
		private String estadoAnimo() {
//			alegria si esta sana y a más de 3 unidades de enfermar: es decir  3 por encima de 5 y 3 por debajo de 50.
			if (energia > 8 || energia < 47)
				return "rrrrrrr";
//			Apatia si está a punto de enfermar ( a menos de tres unidades) , 
			else if (energia > 4 && energia <=8 || (energia >= 47 && energia <=50))
				return "pfffff";
//			para el resto ya está enferma. 
			else if (estado() == Estado.ENFERMA)
				return "ayyyyy";
			else
				return "La mascota ha fallecido";
		}
		public String getNombre() {
			return nombre;
		}
		
		
		
		
		
		
}

