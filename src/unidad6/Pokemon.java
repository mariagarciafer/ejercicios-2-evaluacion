package unidad6;

public class Pokemon {
// 1º Declaro los atributos
	private String nombre;
	private String elemento;
	private int salud;
//	2º Genero el constructor de Pokemons
	public Pokemon(String nombre, String elemento, int salud) {
		
		this.nombre = nombre;
		this.elemento = elemento;
		this.salud = salud;
	}
// 3º 	¿que cosas puede hacer un Pokemon? métodos get y un método set para la salud, porque va variando (el enunciado dice que pierde y gana puntos de salud)
	
	public String getNombre() {
		return nombre;
	}
	public String getElemento() {
		return elemento;
	}
	public int getSalud() {
		return salud;
	}
	public void setSalud(int salud) {
		this.salud = salud;
	}
	@Override
	public String toString() {
		return "Pokemon [nombre=" + nombre + ", elemento=" + elemento + ", salud=" + salud + "]";
	}
	
	
	
}
