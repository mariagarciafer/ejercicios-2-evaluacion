package clases;
// Manejo de excepciones, evitar que el programa se interrumpa cuando se produce un erroro durante la ejecución del programa.
// Debido a un error de  programción, a un fallo de hardware, ....fallos de memoria en el sistem...etc.
// Una excepción es un objeto que se utiliza para poner de manifiesto que ha ocurrido una situación anómala en el programa.

public class ManExcepciones {
	String s1;
	String s2="Hola";
	
	public void unMetodo() {
		System.out.println(s1.charAt(0));
//		Error: s1 tiene el valor null, no se ha iniciado, se declara con una clase, un tipo String, por tanto es una referencia y las referencias se inicializan con el valor null.
//		(NullPointerException)
	}

	public void otroMetodo() {
		System.out.println(s2.charAt(10));
//		Error: s2 sólo tiene 4 caractéres, el índice 10 no existe. 
//		StringIndexOutOfBoundsException: el índice no es menor que la longitud de la cadena. la lanza el método CharAt de la clase String
	}
	public static void main(String[]args) {
		ManExcepciones c = new ManExcepciones();
		int n=1;
//		Capturo la excepción dentro de un bloque try-catch, y todo lo que no quiero que se ejecute si la excepción se lanza,
		try {
			switch(n) {
			case 1 :
				c.unMetodo();
				break;
			case 2 : 
				c.otroMetodo();
			}
		} catch (NullPointerException e) {
//			c.printStackTrace(); Me muestra el recorrido de la excepcion
			System.out.println("Error de puntero nulo");
		} catch (StringIndexOutOfBoundsException e) {
			
			System.out.println("Error limites de la cadena");
		
		} finally {
			System.out.println("Esto se ejecuta siempre");
		}
		System.out.println("Fin del programa");
}
}

//El metodo en el que se lanza la sentencia throw finaliza su ejecución si nadie caprtura la excepcion y esta se propaga al contexto de llamada 
//( al lugar desde donde se llamó a ese método), el cuerpo del método, en este caso otro método