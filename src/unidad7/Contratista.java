package unidad7;

import java.util.ArrayList;

public class Contratista extends Empleado {

	private ArrayList<SociedadAnonima> empresas = new ArrayList<>();


	public Contratista(String nombre, String apellidos, String fecha, String cuenta) {
		super(nombre, apellidos, fecha, cuenta);

	}
	
	public void addEmpresa(SociedadAnonima empresa) {
		empresas.add(empresa);
	}
	
	public void removeEmpresa(SociedadAnonima empresa) {
		empresas.remove(empresa);
	}
	
	public SociedadAnonima [] getEmpresas() {
		return (SociedadAnonima []) empresas.toArray();
//		Hago la conversión de tipos porque toArray retorna un Array de eltos de tipo object, 
//		por tanto lo convierto a elementos de tipo SociedadAnonima
	}

	@Override
	public String toString() {
		return "Contratista [empresas=" + empresas + ", toString()=" + super.toString() + "]";
	}

	@Override
	public void realizarPago(double cantidad) {
		// TODO Auto-generated method stub
		System.out.println("Realizando pago a " + getNombre() + " como contratista" + " de " + cantidad + " €.");
		
	}
	
}
