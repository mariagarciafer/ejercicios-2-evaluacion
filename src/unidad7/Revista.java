package unidad7;

public class Revista extends Publicacion {

	private int dia;
	private int mes;
	private int numero;

	public Revista(int codigo, String titulo, int dia, int mes, int año, int numero) {
		super(codigo, titulo, año);
		// TODO Auto-generated constructor stub
		this.dia = dia;
		this.mes = mes;
		this.numero = numero;
	}


	public int getDia() {
		return dia;
	}

	public int getMes() {
		return mes;
	}

	public int getNumero() {
		return numero;
	}


	@Override
	public String toString() {
		return "Revista [dia=" + dia + ", mes=" + mes + ", numero=" + numero + ", toString()=" + super.toString() + "]";
	}


}
