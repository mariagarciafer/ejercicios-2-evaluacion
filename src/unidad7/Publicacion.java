package unidad7;

public abstract class Publicacion {
	private int codigo;
	private String titulo;
	private int año;


	public Publicacion(int codigo, String titulo, int año) {
		
		this.codigo = codigo;
		this.titulo = titulo;
		this.año = año;
	}

	public int getCodigo() {
		return codigo;
	}

	public String getTitulo() {
		return titulo;
	}

	public int getAño() {
		return año;
	}

	@Override
	public String toString() {
		return "Publicacion [codigo=" + codigo + ", titulo=" + titulo + ", año=" + año + "]";
	}


}
