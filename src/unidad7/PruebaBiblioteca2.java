package unidad7;

import java.util.ArrayList;

public class PruebaBiblioteca2 {

	public static void main(String[] args) {
//		El ArrayList va a contener dentro objetos de tipo publicación.
		ArrayList <Publicacion> publicaciones = new ArrayList<>();
		
		publicaciones.add(new Libro(1, "El Quijote", 1605, "Miguel de Cervantes"));
		publicaciones.add(new Revista(3, "Principia Magazine", 5, 8, 2021, 7));
		
		System.out.println(publicaciones);
		
//		Polimorfismo, no transformo objeto, pero apunta a referencias diferentes.
		Publicacion libro2 =new Libro(2, "Lazarillo de Tormes", 1500, "Anónimo");
		System.out.println(libro2);
		
		for (Publicacion p: publicaciones)
			System.out.println(p.toString());
	}


	}


