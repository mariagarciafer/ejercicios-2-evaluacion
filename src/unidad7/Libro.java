package unidad7;

public class Libro extends Publicacion {
	
	private String autor;
	
	public Libro(int codigo, String titulo, int año, String autor) {
		super(codigo, titulo, año);
		this.autor= autor;
	}

	public String getAutor() {
		return autor;
	}
	
	@Override
	public String toString() {
		return  super.toString() + "Libro [autor=" + autor + "]";
	}
	
}
