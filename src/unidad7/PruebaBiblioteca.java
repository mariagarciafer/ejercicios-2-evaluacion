package unidad7;

public class PruebaBiblioteca {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Libro libro = new Libro(1, "El Quijote", 1605, "Miguel de Cervantes");
		System.out.println(libro.toString());
//		Aqui lo que hago en realidad es System.out.println(libro.toString);
		
		Revista revista = new Revista(3, "Principia Magazine", 5, 8, 2021, 7);
		System.out.println(revista);
//		Polimorfismo, no transformo objeto, pero apunta a referencias diferentes.
		Publicacion libro2 =new Libro(2, "Lazarillo de Tormes", 1500, "Anónimo");
		System.out.println(libro2);
		
		
	}

}
