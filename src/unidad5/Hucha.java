package unidad5;

import java.util.Arrays;

//Revisar
public class Hucha {
	public static final int CINCUENTA = 0;
	public static final int VEINTE = 1;
	public static final int DIEZ = 2;
	public static final int CINCO = 3;
	public static final int DOS = 4;
	public static final int UNO = 5;

	private static int [] tipos= {50,20,10,5,2,1};/*Atributo de clase, será común a todas las huchas...static*/
	//array donde almacenaré la cantidad de moneda de cada tipo que habrá en la hucha
	private int [] desglose  = new int[tipos.length];
	//	inicialmente la hucha está cerrada.
	private boolean abierta = false;
	//	contraseña
	private String contraseña = "";//Inicio la contraseña con la cadena vacia

	//	Creo una hucha vacia.

	public Hucha() {
	}

	public Hucha(boolean abierta, String contraseña) {
		this();/*invoco al constructor de arriba para que me cree el array desglose aqui también*/
		this.abierta = abierta;
		this.contraseña = contraseña;
	}
	// los dos siguientes son prescindibles, si añado en el siguiente las lineas correspondientes.	
	public Hucha(int cincuenta, int veinte, int diez, int cinco, int dos, int uno) {
		desglose[0] = cincuenta;
		desglose[1] = veinte;
		desglose[2] = diez;
		desglose[3] = cinco;
		desglose[4] = dos;
		desglose[5] = uno;
	}
	public Hucha(int cincuenta, int veinte, int diez, int cinco, int dos, int uno, boolean abierta) {
		//		Este invoca al constructor de arriba.
		this(cincuenta, veinte, diez, cinco, dos, uno);
		this.abierta = abierta;
	}
	public Hucha(int cincuenta, int veinte, int diez, int cinco, int dos, int uno, boolean abierta, String contraseña) {
		this(cincuenta, veinte, diez, cinco, dos, uno, abierta);
		this.contraseña = contraseña;
	}

	public Hucha(int cantidad) {
		for (int i=0; i<tipos.length && cantidad > 0; i++) {
			if (cantidad > tipos[i]) {
				desglose[i] = cantidad / tipos[i];
				cantidad %= tipos[i];
			}
		}
	}
	public Hucha(int cantidad, boolean abierta) {
		this(cantidad);
		this.abierta = abierta;
	}

	public Hucha(int cantidad, boolean abierta, String contraseña) {
		this(cantidad, abierta);
		this.contraseña = contraseña;
	}

	//	Al final tengo 8 constructores, no esta mal tener exceso, esta bien porque definimos los límites del objeto.

	//	Métodos para definir la funcionalidad.
	//	Cantidad de monedas o billetes que se almacena de cada clase y valor total en euros:
	//	Añado código que lanze una excepcion propia si meto valores no deseados, por ejemplo un billete de 30 €

	public int getDesglose(int tipo) throws HuchaVaciaException {
		if (abierta)
			return desglose[tipo];
		else 
			throw new HuchaVaciaException();

	}
	public int retirar( int cantidad, int tipo) throws HuchaVaciaException {
		if (abierta) {
			if (desglose[tipo] < cantidad)
				cantidad = desglose[tipo];
			desglose[tipo] -= cantidad;
			return cantidad;
		}
		else
			throw new HuchaVaciaException();
	}
	public int retirar(int cantidad) throws HuchaVaciaException {

		if (abierta) {
			for (int i=0; i<tipos.length && cantidad > 0; i++) {
				if (cantidad > tipos[i]) {
					desglose[i] = cantidad / tipos[i];
					cantidad %= tipos[i];
				}
			}
			return 0; // esto no esta bien
		}
		else
			throw new HuchaVaciaException();
	}
	public void abrir(String contraseña) throws ContraseñaIncorrectaException {
		if (this.contraseña.equals(contraseña))
			abierta = true;
		else
			throw new ContraseñaIncorrectaException();
	}

	public void cerrar(String contraseña) throws ContraseñaIncorrectaException {
		if (this.contraseña.equals(contraseña))
			abierta = false;
		else
			throw new ContraseñaIncorrectaException();
	}


	@Override
	public String toString() {
		return Arrays.toString(desglose);
	}

	
}
