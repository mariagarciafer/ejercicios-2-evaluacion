package unidad5;

import java.time.LocalDate;

public class PruebaAnimal {

	public static void main(String[] args) {
		Animal animal = new Animal("Gato");
		System.out.println(animal.getFecha());
		animal.setNombre("Tabbitha");
//		Con el otro constructor
		Animal animal2 = new Animal ("Kitty", LocalDate.of(2003, 7, 15));
		System.out.println(animal2);
	}

}
