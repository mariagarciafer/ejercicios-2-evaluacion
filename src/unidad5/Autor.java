package unidad5;

public class Autor {
//	Atributos de instancia
	private String nombre;
	private String email;
	private Genero genero;
	
//Constructor que inicializa el estado del objeto con los valores que recibe
//	a través de sus parámetros formales	
	
	public Autor(String nombre, String email, Genero genero) {
		
		this.nombre = nombre;
		this.email = email;
		this.genero = genero;
	}
//	Getters para nombre, email y género de una instancia de la clase. 
	

	public String getNombre() {
		return nombre;
	}
	public String getEmail() {
		return email;
	}

	public Genero getGenero() {
		return genero;
	}

// Setter para cambiar el correo electrónico de las instancias de la clase. 

	
	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return String.format("%s (%s) %s", nombre, genero.toString(), email);
	}


	
}
