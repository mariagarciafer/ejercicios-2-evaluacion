package unidad5;

import java.util.ArrayList;

public class Libro {
	//	Atributos de instancia
	private String titulo;
	private ArrayList<Autor> autores;
	private float precio;
	private int stock;
	public Libro(String titulo, ArrayList<Autor> autores, float precio) {

		this.titulo = titulo;
		this.autores = autores;
		this.precio = precio;
		stock= 0;
		//		o tambien this(titulo, autores, precio, 0);
	}
	public Libro(String titulo, ArrayList<Autor> autores, float precio, int stock) {

		this.titulo = titulo;
		this.autores = autores;
		this.precio = precio;
		this.stock = stock;
	}

	//	getters
	public String getTitulo() {
		return titulo;
	}
	public ArrayList<Autor> getAutores() {
		return autores;
	}
	public float getPrecio() {
		return precio;
	}
	public int getStock() {
		return stock;
	}

	//	Setters

	public void setPrecio(float precio) {
		this.precio = precio;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	@Override
	public String toString() {
		/* "título_libro (autor1, autor2, …) precio € - stock unidades en stock" */
		StringBuilder builder = new StringBuilder();
	
		builder.append(titulo);
		builder.append(", (");
		for (Autor a: autores) {
			builder.append(a.getNombre());
			builder.append(", ");
		}
		builder.append("), ");
		builder.append(precio);
		builder.append(" € - ");
		builder.append(stock);
		builder.append("unidades en stock");
		return builder.toString();
	}



}
