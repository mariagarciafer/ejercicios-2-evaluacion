package unidad5;

import java.time.LocalDate;
// Miembros de clase son atributos y métodos en general.
// Miembros de clase son todos aquellos que se declaran usando el modificador static, si no son de instancia. 
// Atributo de clase: Es similar a una variable global.
// Método de clase: es lo más parecido a un método o función global. 
// Para acceder a los miembros de una clase a través del nombre de la clase NombreClase.NombreAtributo

public class Animal {
//	Declaraci�n de atributos, están inicializados/definidos, ya que tiene un valor por defecto ( el que aparece en el manual de Java), aunque no ponga nada. 
	private String nombre;
	private LocalDate fecha; // Es una clase de la librería estándar de Java.
	
//	Comportamiento del Animal, con métodos y sus parámetros, por ejemplo: 
//	void caminar (double distancia, double direccion) {}
	
//	Métodos constructores, no se especifica tipo de datos de retorno en su definición, pueden tener una serie de parametros formales
//	y tienen el mismo nombre que la clase. Sirve para asignar o definir (para eso se usan los parametros formales) 
//	el estado inicial de un objeto cuando se crea. 
	
	public Animal (String nombre, LocalDate fecha) {
//		Asigno el valor que se recibe en los par�metros para asignarlo a los atributos correspondientes( this.nombre el atributo, dcha el parámetro del m�todo)
		this.nombre=nombre;
		this.fecha=fecha;
//		this: palabra reservada, hace referencia a un objeto de la clase Animal
	}
//		Sobrecarga de métodos, diferente número o tipo de parémetros formales.
	
	public Animal(String nombre) {
		this.nombre=nombre;
		fecha=LocalDate.now();//método estético now de la clase LocalDate
//		También se puede hacer en el meno source: Generate constructor using fields y selecciono los atributos/campos
		
//		Modificadores de acceso, se aplican tanto en la declaración de atributos como de métodos. 
//		Se usa el control de acceso a miembros de una clase para proteger la integridad de los objetos. 
		
//		public: se accede a el desde cualquier sitio.
//		protected: sólo desde la propia clase y desde las subclases.
//		package: se aplica este si no especifico ninguno en la declaración de variables o métodos.
//					Desde el interior de la clase y aquellas que pertenecen al mismo paquete.
//		private: solo se puede acceder desde el interior de la clase.
//		
		
	}
//	Setters modifican el valor de los atributos que describen el estado de un atributo.
//	Getters para obtener el valor de los atributos.
	
//  Es habitual hacer un método get y set para cada uno de los atributos de la clase. 
//	Source> Generate Getters and Setters
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public LocalDate getFecha() {
		return fecha;
	}
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	
//		Redefinir el méodo toString( método que se usa para retornar 
//		una cadena que contiene informacién acerca del estado del objeto, 
//		o una parte del estado de un objeto que yo quiera mostrar de forma textual.)
	
	@Override 
	
	public String toString() {
		int edad = LocalDate.now().getYear() - fecha.getYear();
		return "Nombre: " + nombre + "- edad:" + edad  ;
	}
//	Todas las clases creadas en Java heredan por defecto de la clase "Object", para redefinir se usa el toString, 
//	por eso aparece el @Overrride si se hace automaticamente.
}
