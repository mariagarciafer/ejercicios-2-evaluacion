package unidad5;

public class PruebaHucha {

	public static void main(String[] args) {
		//Cliente de la clase Hucha

		Hucha hucha = new Hucha(325);
		try {
			hucha.abrir("a");
			System.out.println(hucha.getDesglose(Hucha.DIEZ));
		} catch (ContraseñaIncorrectaException e) {
			System.out.println("Contraseña incorrecta");
		} catch (HuchaVaciaException e) {
			System.out.println("La hucha está cerrada");
		}
	}

}
